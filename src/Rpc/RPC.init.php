<?php

/**
 * import of Rpc classes
 */

if (defined("RPC_INIT_LOADED")) {
	return;
}

define("RPC_INIT_LOADED", true);

require(dirname(__FILE__)."/RPCClient.php");
require(dirname(__FILE__)."/RPCClientException.php");
require(dirname(__FILE__)."/RPCFault.php");
require(dirname(__FILE__)."/RPCServer.php");
require(dirname(__FILE__)."/RPCServerException.php");
require(dirname(__FILE__)."/RPCServerRequest.php");
require(dirname(__FILE__)."/RPCServerReactRequest.php");
require(dirname(__FILE__)."/RPCServerResponse.php");
require(dirname(__FILE__)."/RPCServerReactResponse.php");
require(dirname(__FILE__)."/RPCServerTemplate.php");
require(dirname(__FILE__)."/RPCServerTypeChecker.php");
