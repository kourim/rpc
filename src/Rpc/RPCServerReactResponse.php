<?php
namespace Rpc\Server;

class RPCServerReactResponse extends RPCServerResponse
{

	/**
	 * React response
	 * @var mixed
	 */
	private $reactResponse = null;

	/**
	 * RPCServerReactResponse constructor.
	 * @param $response
	 */
	public function __construct($response)
	{
		$this->reactResponse = $response;
	}

	/**
	 * @param string $statusString
	 * @param string[] $headers
	 */
	public function writeHeaders($statusString, $headers = [])
	{
		list($statusCode,) = explode(" ", $statusString, 2);
		$this->reactResponse->writeHead($statusCode, $headers);
	}

	/**
	 * @param string $data
	 */
	public function write($data)
	{
		ob_start();
		$this->jsonEncode($data);
		$tmp = ob_get_contents();
		ob_end_clean();
		$this->reactResponse->end($tmp);
	}

	/**
	 * @param mixed $data
	 */
	protected function output($data)
	{
		echo $data;
	}

}
