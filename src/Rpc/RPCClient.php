<?php
namespace Rpc\Client;

/**
 *
 * @author kourim
 *
 * Class RPCClient je univerzalni klient pro odesilani request na RPC server
 *
 * Vlastnosti
 *  - klient se vzdy pta ve formatu JSON
 *  - cela trida se chova jako objekt, ze ktereho lze potom rovnou volat fce nadefinovane na RPC serveru
 *
 */
class RPCClient
{

	/**
	 * @var string|null
	 */
	private $url = null;

	/**
	 * @var int
	 */
	private $timeout = 10;

	/**
	 * RPCClient constructor
	 * @param string $url
	 * @param array $params
	 */
	public function __construct($url, $params = array())
	{
		//url adresa kam se budeme ptat
		$this->url = rtrim($url, '/').'/';

		//nastavime si timeout
		if (isset($params['timeout']) and ctype_digit((string)$params['timeout'])) {
			$this->timeout = $params['timeout'];
		}
	}

	/**
	 * magicka funkce na volani metod, aby se rpc rozhrani tvarilo jako metody objektu
	 * @param string $name
	 * @param array $arguments
	 * @throws RPCClientException
	 * @return mixed
	 */
	public function __call($name, $arguments)
	{
		//je potreba z argumentu sebrat jedno pole
		if (is_array($arguments) and !empty($arguments)) {
			$arguments = reset($arguments);
		} else {
			$arguments = array();
		}

		//zavolame a zpracujeme si vystup
		$data = $this->RPCCall($this->url.$name, $arguments);
		if (isset($data->status, $data->$name) and $data->status == '200') {
			return $data->$name;
		} elseif (isset($data->status, $data->status_message)) {
			throw new RPCClientException($data->status_message, $data->status);
		} else {
			throw new RPCClientException('Response in unknown format from server', 0);
		}
	}

	/**
	 * funkce vezme vstupni data a preda je na vyslednou adresu, vyzvedne si data,
	 * prozene je zpatky pres json a vrati vysledna data
	 * @param string $url
	 * @param mixed $data
	 * @throws RPCClientException
	 * @return mixed
	 */
	private function RPCCall($url, $data)
	{
		//pokud se nam nepovede vyrobit json
		if (!empty($data)) {
			if (!$data = json_encode($data)) {
				throw new RPCClientException('Cannot encode data to json format');
			}
		}

		//zavolame si pro data
		$ch = curl_init();
		if (!empty($data)) {
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_POST => true,
				CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POSTFIELDS => $data,
				CURLOPT_TIMEOUT => $this->timeout,
			));
		} else {
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_TIMEOUT => $this->timeout,
			));
		}
		$result = curl_exec($ch);
		curl_close($ch);

		//pokud nemame data v jsonu
		if (!$result = json_decode($result)) {
			throw new RPCClientException('Cannot decode json data from '.$url);
		}

		return $result;
	}

}
