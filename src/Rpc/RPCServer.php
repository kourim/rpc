<?php
namespace Rpc\Server;

use Exception;
use ReflectionClass;
use ReflectionMethod;
use stdClass;

/**
 *
 * @author kourim
 *
 * Class RPCServer je univerzalni server pro zpracovani GET/POST/JSON requestu
 *
 * Vlastnosti
 *  - server odpovida ve formatu JSON
 *  - metoda se zavola jako HTTP_PATH/metoda(napr. http://localhost/rpcserver/metoda) a pro vstupni data lze pouzit 3 metody:
 *  	- pokud je odeslana hlavicka Content-Type s hodnotou 'xml/json', tak se data hledaji v HTTP_RAW_POST_DATA a format je ocekavan json
 *  	- pokud je odeslana hlavicka Content-Type s hodnotou 'application/x-www-form-urlencoded', ta se data hledaji v POSTu
 *  	- ve vsech ostatnich pripadech se data hledaji v GETu
 *  - po inicializaci je potreba priradit tridu, ve ktere jsou nadefinovane nejake public metody, ktere se timto automaticky zpristupni ven
 *  - v prirazene tride je potreba mit vsechny soubory zdokumentovane za pomoci phpDocu, z techto udaju se pak nasledne tahaji helpy a kontroluji se podle toho vstupni promenne!!!
 *  - prirazena trida musi dedit z objektu RPCServerTemplate
 *  - vstupni parametry jsou kontrolovany vuci parametrum funkce a to i typum, pokud se na vstupu funkce vyzaduje objekt, je tento objekt nejprve vytvoren a pak predan do funkce
 *  - json_encode funkce je prepsana pro nase potreby, puvodni php funkce se nepouziva z duvodu, ze nezvlada lazy-loading objekty
 *  - server obsahuje systemove funkce system_list_methods, ktera vraci vsechny pristupne funkce na rozhrani, a system_method_help, ktera vraci help k funkci
 *
 */
class RPCServer
{

	/**
	 * @var string|null
	 */
	private $class = null;
	/**
	 * @var string[]
	 */
	private $serverMethods = array();

	/**
	 * @var bool
	 */
	private $jsonDecodeAssoc = false;

	/**
	 * @var RPCServerRequest
	 */
	private $request = null;

	/**
	 * @var RPCServerResponse
	 */
	private $response = null;

	/**
	 * RPCServer constructor.
	 * @param array $options
	 */
	public function __construct($options = array())
	{
		if (isset($options['assoc_array'])) {
			$this->jsonDecodeAssoc = (bool)$options['assoc_array'];
		}

		if (isset($options['request']) and $options['request'] instanceof RPCServerRequest) {
			$this->request = $options['request'];
		} else {
			$this->request = new RPCServerRequest();
		}

		if (isset($options['response']) and $options['response'] instanceof RPCServerResponse) {
			$this->response = $options['response'];
		} else {
			$this->response = new RPCServerResponse();
		}
	}

	/**
	 * RPCServer destructor
	 */
	public function __destruct()
	{
	}

	/**
	 * funkce priradi pro server danou classu, ve kterem se pak budou hledat funkce k pousteni,
	 * je vyzadovano aby classa mela alespon jednu public metodu, ktera bude vystavena a musi byt podedena
	 * od tridy RPCServerTemplate
	 * pokusi se rovnou rozparsovat informace o dane classe a ty si pak nasledne ulozi do promenne $serverMethods
	 * @param string $className
	 * @throws RPCServerException
	 */
	public function setClass($className)
	{
		if (!class_exists($className)) {
			throw new RPCServerException("Class '".$className."' doesn't exist", 1);
		}

		//zkontrolujeme jestli objekt je potomek nasi tridy
		if (!is_subclass_of($className, 'RPCServerTemplate')
			and !is_subclass_of($className, 'Rpc\Server\RPCServerTemplate')
		) {
			throw new RPCServerException("Class '".$className."' must be inherited from class 'RPCServerTemplate'", 4);
		}

		$this->class = $className;
		$this->serverMethods = $this->parseClass($this->class);

		if (empty($this->serverMethods)) {
			throw new RPCServerException("Class '".$className."' hasn't any public methods", 2);
		}
	}

	/**
	 * funkce zpracuje vstup, priradi ho i s parametry na spravnou funkci a da na vystup odpoved pres RPCServerResponse
	 * @throws RPCServerException
	 */
	public function handle()
	{
		try {
			//pro nadefinovanou classu si udelame tabulku s tim jake mame fce
			if (is_null($this->class)) {
				throw new RPCServerException("You must define class first before call handle", 3);
			}

			//rozparsujeme si cestu a najdeme si z toho metodu, pokud neexistuje
			$method = $this->request->getMethod();
			if (!array_key_exists($method, $this->serverMethods)) {
				throw new RPCServerException("RPC method '".$method."' not found", 404);
			}

			//najdeme si vstupni data
			$data = null;
			$data = $this->request->getData();

			//naparujeme si vstupni data na metodu
			$objectClass = new $this->class();

			$variables = array();

			if (!is_null($data)) {
				foreach ($this->serverMethods[$method]['variables'] as $varName => $var) {
					//pokud promenna neexistuje v requestu a mi ji vyzadujeme tak budeme kricet
					if (!isset($data->$varName) and !$var['optional']) {
						throw new RPCServerException("Missing argument '".$varName."'", 400);
					//pokud neexistuje a je optional tak si vyplnime default hodnotu
					} elseif (!isset($data->$varName) and $var['optional']) {
						$data->$varName = $var['defaultValue'];
					}

					//otestujeme jeji typ v pripade ze neni null nebo je povinna
					if (!$var['optional'] or !is_null($data->$varName)) {
						switch ($var['type']) {
							case 'int':
								if (!RPCServerTypeChecker::isInteger($data->$varName)) {
									throw new RPCServerException("Wrong value for argument '".$varName."', expected '".$var['type']."', received '".RPCServerTypeChecker::getType($data->$varName)."'", 400);
								}
								break;
							case 'float':
								if (!RPCServerTypeChecker::isFloat($data->$varName)) {
									throw new RPCServerException("Wrong value for argument '".$varName."', expected '".$var['type']."', received '".RPCServerTypeChecker::getType($data->$varName)."'", 400);
								}
								break;
							case 'bool':
								if (!RPCServerTypeChecker::isBoolean($data->$varName)) {
									throw new RPCServerException("Wrong value for argument '".$varName."', expected '".$var['type']."', received '".RPCServerTypeChecker::getType($data->$varName)."'", 400);
								}
								break;
							case 'string':
								if (!RPCServerTypeChecker::isString($data->$varName)) {
									throw new RPCServerException("Wrong value for argument '".$varName."', expected '".$var['type']."', received '".RPCServerTypeChecker::getType($data->$varName)."'", 400);
								}
								break;
							case 'array':
								if (!RPCServerTypeChecker::isArray($data->$varName)) {
									throw new RPCServerException("Wrong value for argument '".$varName."', expected '".$var['type']."', received '".RPCServerTypeChecker::getType($data->$varName)."'", 400);
								}
								break;
							case 'unknown':
							case 'mixed':
								//pokud je mixed tak muze obsahovat cokoliv, je nam tedy jedno co se preda
								break;
							case 'object':
								if (!RPCServerTypeChecker::isObject($data->$varName)) {
									throw new RPCServerException("Wrong value for argument '".$varName."', expected '".$var['type']."', received '".RPCServerTypeChecker::getType($data->$varName)."'", 400);
								}
								break;
							default:
								//pokud classa kterou chceme pridat do fce neexistuje
								if (!class_exists($var['type'])) {
									throw new RPCServerException("Class '".$var['type']."' doesn't exist, cannot create object for function variables", 5);
								}

								try {
									$data->$varName = new $var['type']($data->$varName);
								} catch (Exception $e) {
								}

								//pokud se nam objekt nevytvoril
								if (!is_a($data->$varName, $var['type'])) {
									throw new RPCServerException("cannot create object '".$var['type']."' for function variables", 6);
								}
								break;
						}
					}

					$variables[] = $data->$varName;
				}
			}

			//pokud chceme predavat jako pole, tak rekurzivne prolezem a vsechno pretypujem
			if ($this->jsonDecodeAssoc) {
				$variables = json_decode(json_encode($variables), true);
			}

			//zavolame si fci
			$returnData = call_user_func_array(array($objectClass, $method), $variables);

			//z vyslednych dat vytvorime output a ten dame na vystup
			$this->createResponse($method, $returnData);
		} catch (RPCServerException $e) {
			if ($e->getCode() >= 300) {
				$this->createErrorResponse($e->getCode(), $e->getMessage());
			} else {
				error_log('RPCServer error ('.$e->getCode().'): '.$e->getMessage());
				$this->createErrorResponse(500, 'Internal Server Error');
			}
		//pokud mame vyvolanou rizenou chybu z aplikace
		} catch (RPCFault $e) {
			$this->createErrorResponse($e->getStatusCode(), $e->getMessage());
		//pokud mi odkudkoliv vyskoci jakakoliv exception, tak je to spatne
		} catch (Exception $e) {
			$this->createErrorResponse(500, 'Internal Server Error');
		}
	}

	/**
	 * funkce rozparsuje danou classu a vrati pole obsahujici metody s jejich komentari a vstupnimi parametry
	 * @param string $className
	 * @return array
	 */
	private function parseClass($className)
	{
		$classMethods = array();
		$class = new ReflectionClass($className);

		//vytahneme si vsechny public metody, ktera nam trida nabizi ke spusteni
		$methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
		foreach ($methods as $method) {
			$function = new ReflectionMethod($className, $method->name);

			$classMethods[$method->name] = array(
				'phpdoc' => ($function->getDocComment() ? $function->getDocComment() : null),
				'variables' => array(),
			);

			//najdeme si vsechny promenne s jejich typy v phpdocu, pokud existuje
			$variables = array();
			if (!is_null($classMethods[$method->name]['phpdoc'])) {
				preg_match_all(
					'~\*\s*@param\s*([a-z]+)\s*\$([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)~',
					$classMethods[$method->name]['phpdoc'],
					$matches
				);
				if (!empty($matches[0])) {
					$count = count($matches[0]);
					for ($i = 0; $i < $count; $i++) {
						$variables[$matches[2][$i]] = $matches[1][$i];
					}
				}
			}

			//pro vsechny parametry ted najdeme co je treba a co nezname zkusime doplnit z phpdocu
			$params = $function->getParameters();
			foreach ($params as $param) {
				$classMethods[$method->name]['variables'][$param->name] = array(
					'type' => (isset($param->getClass()->name)
						? $param->getClass()->name
						: (isset($variables[$param->name]) ? $variables[$param->name] : 'mixed')
					),
					'optional' => ($param->isOptional() ? true : false),
					'defaultValue' => ($param->isOptional() ? $param->getDefaultValue() : null),
				);
			}
		}

		return $classMethods;
	}

	/**
	 * funkce dava na vystup zformatovany vystup RPCServeru
	 * @param string $method
	 * @param mixed $data
	 */
	private function createResponse($method, $data)
	{
		$this->response->writeHeaders(
			"200 OK",
			[
				'Content-Type' => 'application/json; charset=utf-8',
				'Access-Control-Allow-Origin' => '*',
			]
		);
		$this->response->write([
			'status' => 200,
			'status_message' => 'OK',
			$method => $data,
		]);
	}

	/**
	 * funkce dava na vystup zformatovany chybovy vystup RPCServeru
	 * @param int $errorCode
	 * @param string $errorMessage
	 */
	private function createErrorResponse($errorCode, $errorMessage)
	{
		//vratime prislusny hlavicky
		switch ($errorCode) {
			case 300:
				$headerString = '300 Multiple Choices';
				break;
			case 301:
				$headerString = '301 Moved Permanently';
				break;
			case 302:
				$headerString = '302 Moved Temporarily';
				break;
			case 303:
				$headerString = '303 See Other';
				break;
			case 304:
				$headerString = '304 Not Modified';
				break;
			case 305:
				$headerString = '305 Use Proxy';
				break;
			case 400:
				$headerString = '400 Bad Request';
				break;
			case 401:
				$headerString = '401 Unauthorized';
				break;
			case 402:
				$headerString = '402 Payment Required';
				break;
			case 403:
				$headerString = '403 Forbidden';
				break;
			case 404:
				$headerString = '404 Not Found';
				break;
			case 405:
				$headerString = '405 Method Not Allowed';
				break;
			case 406:
				$headerString = '406 Not Acceptable';
				break;
			case 407:
				$headerString = '407 Proxy Authentication Required';
				break;
			case 408:
				$headerString = '408 Request Time-out';
				break;
			case 409:
				$headerString = '409 Conflict';
				break;
			case 410:
				$headerString = '410 Gone';
				break;
			case 411:
				$headerString = '411 Length Required';
				break;
			case 412:
				$headerString = '412 Precondition Failed';
				break;
			case 413:
				$headerString = '413 Request Entity Too Large';
				break;
			case 414:
				$headerString = '414 Request-URI Too Large';
				break;
			case 415:
				$headerString = '415 Unsupported Media Type';
				break;
			case 500:
				$headerString = '500 Internal Server Error';
				break;
			case 501:
				$headerString = '501 Not Implemented';
				break;
			case 502:
				$headerString = '502 Bad Gateway';
				break;
			case 503:
				$headerString = '503 Service Unavailable';
				break;
			case 504:
				$headerString = '504 Gateway Time-out';
				break;
			case 505:
				$headerString = '505 HTTP Version not supported';
				break;
			default:
				$headerString = '200 OK';
				break;
		}

		$this->response->writeHeaders(
			$headerString,
			['Content-Type' => 'application/json; charset=utf-8']
		);
		$this->response->write([
			'status' => $errorCode,
			'status_message' => $errorMessage,
		]);
	}

}
