<?php
namespace Rpc\Server;

use stdClass;

class RPCServerRequest
{

	/**
	 * @return string
	 */
	public function getMethod()
	{
		return basename(substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'].'?', '?')));
	}

	/**
	 * @return array
	 */
	public function getHeaders()
	{
		return apache_request_headers();
	}

	/**
	 * funkce se pokusi najit data na jednom z mist, kde je ocekava a vrati je ve strukture
	 * @throws RPCServerException
	 * @return object
	 */
	public function getData()
	{
		//zjistime si jestli je odeslan nejaky contentType v requestu
		$contentType = null;
		if ($tmp = $this->getHeaders()) {
			foreach ($tmp as $header => $value) {
				if (strtolower($header) == 'content-type') {
					$contentType = $value;
				}
			}
		}

		$data = new stdClass();

		//podle contentTypu se rozhodnem odkud data berem
		switch ($contentType) {
			//pokud jsou data odeslany pres formular v POSTu
			case 'application/x-www-form-urlencoded':
				if (isset($_POST)) {
					foreach ($_POST as $key => $value) {
						$data->$key = $value;
					}
				}
				break;
			//pokud jsou data odeslany v jsonu
			case 'application/json':
				//sahneme si pro data do raw postu
				$requestData = null;
				if (isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
					$requestData = $GLOBALS['HTTP_RAW_POST_DATA'];
				} elseif ($tmp = file_get_contents('php://input') and !empty($tmp)) {
					$requestData = $tmp;
				}

				//pokud jsme nasli nejaka data, tak je rozparsujeme
				if (!is_null($requestData)) {
					if (!$data = json_decode($requestData)) {
						throw new RPCServerException('Cannot parse json data', 5);
					}
				}
				break;
			//defaultne data hledame v GETu
			default:
				if (isset($_GET)) {
					foreach ($_GET as $key => $value) {
						$data->$key = $value;
					}
				}
				break;
		}

		return $data;
	}

}
