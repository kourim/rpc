<?php
namespace Rpc\Server;

use Exception;

class RPCFault extends Exception
{

	/**
	 * @var int
	 */
	public $statusCode = 200;

	/**
	 * RPCFault constructor.
	 * @param string|null $message
	 * @param int|null $code
	 * @param int|null $statusCode
	 * @param Exception|null $previous
	 */
	public function __construct($message = null, $code = null, $statusCode = null, $previous = null)
	{
		parent::__construct($message, $code, $previous);
		$this->statusCode = $statusCode;
	}

	/**
	 * @return int
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}

}
