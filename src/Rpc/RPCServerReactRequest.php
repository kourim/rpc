<?php
namespace Rpc\Server;

use stdClass;

class RPCServerReactRequest extends RPCServerRequest
{

	/**
	 * React response
	 * @var mixed
	 */
	private $reactRequest = null;

	/**
	 * POST request body
	 * @var string
	 */
	private $reactRequestBody = null;

	/**
	 * RPCServerReactRequest constructor.
	 * @param $request
	 * @param $requestBody
	 */
	public function __construct($request, $requestBody = null)
	{
		$this->reactRequest = $request;
		$this->reactRequestBody = $requestBody;
	}

	/**
	 * @return string
	 */
	public function getMethod()
	{
		return basename(
			substr($this->reactRequest->getPath(), 0, strpos($this->reactRequest->getPath().'?', '?'))
		);
	}

	/**
	 * @return array
	 */
	public function getHeaders()
	{
		return $this->reactRequest->getHeaders();
	}

	/**
	 * funkce se pokusi najit data na jednom z mist, kde je ocekava a vrati je ve strukture
	 * @throws RPCServerException
	 * @return object
	 */
	public function getData()
	{
		//zjistime si jestli je odeslan nejaky contentType v requestu
		$contentType = null;
		if ($tmp = $this->getHeaders()) {
			foreach ($tmp as $header => $value) {
				if (strtolower($header) == 'content-type') {
					$contentType = $value;
				}
			}
		}

		$data = new stdClass();
		//podle contentTypu se rozhodnem odkud data berem
		switch ($contentType) {
			//pokud jsou data odeslany v jsonu, jediny mozny reseni
			case 'application/json':
				//pokud jsme nasli nejaka data, tak je rozparsujeme
				if (!is_null($this->reactRequestBody)) {
					if (!$data = json_decode($this->reactRequestBody)) {
						throw new RPCServerException('Cannot parse json data', 5);
					}
				}
				break;
			default:
				throw new RPCServerException("Data must be send in json through POST");
		}
		return $data;
	}

}
