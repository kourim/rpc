<?php
namespace Rpc\Server;

/**
 * trida obsahujici pouze checkery na jednotlive datove typy, co ma phpko
 */
class RPCServerTypeChecker
{

	/**
	 * @param mixed $var
	 * @return bool
	 */
	public static function isInteger($var)
	{
		return (ctype_digit((string)$var) and $var <= PHP_INT_MAX and $var >= - PHP_INT_MAX);
	}

	/**
	 * @param mixed $var
	 * @return bool
	 */
	public static function isFloat($var)
	{
		return is_numeric($var);
	}

	/**
	 * @param mixed $var
	 * @return bool
	 */
	public static function isString($var)
	{
		return is_string($var);
	}

	/**
	 * @param mixed $var
	 * @return bool
	 */
	public static function isArray($var)
	{
		return is_array($var);
	}

	/**
	 * @param mixed $var
	 * @return bool
	 */
	public static function isBoolean($var)
	{
		return is_bool($var);
	}

	/**
	 * @param mixed $var
	 * @return bool
	 */
	public static function isObject($var)
	{
		return is_object($var);
	}

	/**
	 * @param mixed $var
	 * @return bool
	 */
	public static function isClass($var, $className)
	{
		return is_a($var, $className);
	}

	/**
	 * @param mixed $var
	 * @return string
	 */
	public static function getType($var)
	{
		return gettype($var);
	}

}
