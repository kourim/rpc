<?php
namespace Rpc\Server;

class RPCServerResponse
{

	/**
	 * write headers to client
	 * @param string $statusString
	 * @param string[] $headers
	 */
	public function writeHeaders($statusString, $headers = [])
	{
		header((isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0').' '.$statusString);
		foreach ($headers as $header => $value) {
			header($header.": ".$value);
		}
	}

	/**
	 * write output data to client
	 * @param mixed $data
	 */
	public function write($data)
	{
		$this->jsonEncode($data);
	}

	/**
	 * @param mixed $data
	 */
	protected function output($data)
	{
		echo $data;
	}

	/**
	 * funkce na generovani json vystupu
	 * umi si poradit i s lazy-load objektem, u toho je potreba definovat metodu isSimpleArray, podle ktere pozna,
	 * zda se jedna o asociativni pole nebo jednoduche pole
	 * @param mixed $val
	 * @param bool $write
	 * @return string
	 */
	protected function jsonEncode($val, $write = true)
	{
		//pokud uz jsme u nejakych finalnich hodnot tak je vratime
		if (is_string($val)) {
			$this->output(json_encode($val));
			return;
		}
		if (is_numeric($val)) {
			$this->output($val);
			return;
		}
		if ($val === null) {
			$this->output('null');
			return;
		}
		if ($val === true) {
			$this->output('true');
			return;
		}
		if ($val === false) {
			$this->output('false');
			return;
		}

		$assoc = false;
		//koukneme se jestli nahodou nemuze objekt reprezentovat simpleArray
		if (is_object($val)) {
			$assoc = true;
			if (method_exists($val, 'isSimpleArray')) {
				$assoc = !$val->isSimpleArray();
			}
			//koyz je pole tak koukneme jestli je associativni nebo ne
		} elseif (is_array($val)) {
			$array = array_keys($val);
			$assoc = ($array !== array_keys($array));
			unset($array);
		}

		//tohle pujde ven
		$first = true;
		$this->output($assoc ? '{' : '[');
		foreach ($val as $key => $value) {
			//prvni prvek pred sebe nedava carku
			$this->output($first ? '' : ',');
			$first = false;

			//k asociativnimu poli pridam jeste klic
			$this->output($assoc ? json_encode((string)$key).':' : '');
			self::jsonEncode($value);
		}
		$this->output($assoc ? '}' : ']');
	}

}
