<?php
namespace Rpc\Server;

use Exception;
use ReflectionClass;
use ReflectionMethod;

/**
 * od tehle tridy by meli dedit vsechny classy pro RPC rozhrani
 */
abstract class RPCServerTemplate
{

	/**
	 * funkce nam vrati seznam vsech public metod na dane tride
	 * tyto tridy je pak mozne zavolat pres RPC rozhrani
	 * @return array
	 */
	final public function system_list_methods()
	{
		$class = new ReflectionClass($this);

		$out = array();
		foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
			$out[] = $method->name;
		}

		return $out;
	}

	/**
	 * funkce vrati pro danou metodu jeji napovedu
	 * @param string $method
	 * @return string
	 */
	final public function system_method_help($method)
	{
		try {
			$func = new ReflectionMethod($this, $method);
			return ($func->getDocComment() ? $func->getDocComment() : '');
		} catch (Exception $e) {
			return '';
		}
	}

}
